DOWNLOAD_URL="https://firebase.tools/bin/linux/latest"
INSTALL_DIR="/usr/local/bin"

echo "-- Downloading binary..."
curl -o "$INSTALL_DIR/firebase" -L --progress-bar $DOWNLOAD_URL

# Once the download is complete, we mark the binary file as readable
# and executable (+rx).

echo "-- Setting permissions on binary..."
chmod +rx "$INSTALL_DIR/firebase"
