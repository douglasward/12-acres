var navToggle = document.querySelector('.nav-toggle'),
  nav = document.querySelector('.nav'),
  body = document.querySelector('body');

doToggle = function(e, prevent) {
  if (prevent) e.stopPropagation();
  this.navToggle.classList.toggle('expanded');
  this.nav.classList.toggle('expanded');
};

document.querySelector('nav').classList.add('js');
navToggle.addEventListener('click', function(e) {
  doToggle(e, true);
});
body.addEventListener('click', function(e) {
  doToggle(e);
});
