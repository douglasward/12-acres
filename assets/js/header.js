var imgEl = document.querySelector('header img.img-large');

if (imgEl) {
  imgEl.classList.remove('loaded');

  window.onload = function() {
    var imgLarge = new Image();
    imgLarge.src = imgEl.currentSrc;
    imgLarge.onload = function() {
      imgEl.classList.add('loaded');
    };
  };
}
