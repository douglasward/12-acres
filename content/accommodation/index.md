---
title: the barn
menu:
  main:
    name: accommodation
header_image: 12acres.jpg
url: '/accommodation/barn'
---

{{< section >}}

#### The barn is a beautiful stone built cottage set in a steep, secluded valley in the Welsh borders

The barn is in a sheltered spot above a wooded stream and on a quite single track lane.

The accommodation includes an open plan ground floor and 3 generously sized bedrooms.

{{< /section >}}

{{< section class="primary text-align-left" img="the-barn.jpg" >}}


#### 9 Guests | 3 Bedrooms | 7 beds | 3 bathrooms

**Entire home:** you’ll have the whole barn to yourself

**Great location:** the barn is situated...

{{< /section >}}

{{< section class="secondary reverse text-align-left" img="the-barn.jpg" >}}

### Amenities

- WiFi
- Kitchen
- Smoke detectors
- Carbon monoxide detectors
- Hangers

{{< /section >}}

<!-- ...more

- Basic
  - Wifi
  - Continuous access in the listing
  - Iron
  - Central heating
- Essentials
  -Towels, bed sheets, soap and toilet paper
  - Hot water
- Facilities
  - Free parking
- Dining
  - Kitchen
  - Cooking basics
  - Pots and pans, oil, salt and pepper
  - Refrigerator
  - Stove
- Bed and bath
  - Hangers
  - Hair dryer
  - Shampoo
  - Bed linen
  - Extra pillows and blankets
- Outdoor
  - Patio
  - Access to surrounding fields and grassed area
- Safety features
  - Fire extinguisher
  - First aid kit -->

{{< section class="primary text-align-left" img="the-barn.jpg" >}}

### Sleeping arrangements

**Bedroom 1:** 1 double bed, 2 single beds and ensuite

**Bedroom 2:** 1 double bed and 1 single bed

**Bedroom 3:** 2 single beds and ensuite

{{< /section >}}

<!-- ## Accessibility

?

## Reviews

?

## The area

?

## Things to keep in mind

Check-in: After 1PM
Checkout: 10AM

#### House Rules

- No pets
- No parties or events
- Smoking is allowed
 -->
