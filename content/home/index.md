---
title: twelve acres
header_image: 12acres.jpg
menu:
  main:
    name: home
    weight: 10
---

{{< section >}}

#### Stunning barn accommodation in a magical, secluded, welsh valley

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{{< /section >}}

{{< section img="/images/test.jpg" class="primary" >}}

#### Stunning barn accommodation in a magical, , welsh valley

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{{< /section >}}

{{< section img="/images/test.jpg" class="secondary reverse" >}}

#### Stunning barn accommodation in a magical, , welsh valley

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{{< /section >}}
